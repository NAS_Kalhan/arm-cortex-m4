#define clock_portF (*((unsigned long volatile *)0x400FE108))
#define data_portF  (*((unsigned long volatile *)0x400253FC))



typedef  struct {
 
	//unsigned long* clock;             // clock 
	unsigned long* unlock;
	unsigned long* allow_changes;
	unsigned long* disable_analog;
	unsigned long* multiplex;        //PCTL
	unsigned long* direction;
	unsigned long* noalternate;      //AFSEL
	unsigned long* enable_pullup;    //PUR
	//unsigned long* enable_pulldown;	
	unsigned long* digital_enable;
	//unsigned long* data;


}ports;

ports portF ={
	
	//(unsigned long volatile *)(0x400FE108),
	(unsigned long volatile *)(0x40025520),
	(unsigned long volatile *)(0x40025524),
	(unsigned long volatile *)(0x40025528),
	(unsigned long volatile *)(0x4002552C),
  (unsigned long volatile *)(0x40025400),
  (unsigned long volatile *)(0x40025420),
  (unsigned long volatile *)(0x40025510),
  (unsigned long volatile *)(0x4002551C),
	//(unsigned long volatile *)(0x400253FC)

};

void make_Digi_input_output(ports *ptr, unsigned long in_mask,unsigned long out_mask){
	volatile unsigned long delay;
	unsigned long mask=0;
	
	 mask = in_mask | out_mask;
	
	/*(*ptr->clock)   	|= 0x00000020;
							delay  = (*ptr->clock);*/
	clock_portF |= 0x00000020;
	      delay  = clock_portF;
	
	
	if(mask&0x01){
		(*ptr->unlock)   = 0x4C4F434B;}
	(*ptr->allow_changes) |= mask;
	(*ptr->disable_analog) = 0x00;
	(*ptr->multiplex)      = 0x00000000;
	(*ptr->direction)      = (((*ptr->direction) & (~in_mask))| out_mask);	
	(*ptr->noalternate)     =  0x00;	
  (*ptr->enable_pullup)   =	(in_mask);	
	(*ptr->digital_enable) |=	(mask);		
	

}

void out_data(ports *ptr,unsigned long mask,unsigned long status){
  if(status ==1){
	//(*ptr->data) |= mask;  //  high
		data_portF |= mask;}
		
	else if(status ==0) {
	 //(*ptr->data) &=(~mask); //low
		data_portF &=(~mask);
	}
  else{ 
		//(*ptr->data) ^=(mask); //Trogle
		data_portF ^=(mask);
	 }
}

unsigned long read_data_if_pull_up(ports *ptr,unsigned long mask){

 return   (data_portF & mask) ;                //(~(*ptr->data) & mask);  

}