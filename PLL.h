/* 
To enable PLL 80Hz clock for Lunchpad
by referning page No: 231,245,225,254,260
written by   : N.A.S Kalhan
written date : 13/6/2021

*/
typedef const struct {

unsigned long *RCC2; 
unsigned long *RCC; 	
unsigned long *RIS;
	
}PLL;

PLL register_address ={
		(unsigned long volatile *)(0x400FE070),
		(unsigned long volatile *)(0x400FE060),
		(unsigned long volatile *)(0x400FE050)
};

PLL *ptr =&register_address; 

void PLL_init(void){
*ptr->RCC2 |= (1<<31) ;  //0x80000000
*ptr->RCC2 |= (1<<11) ;  //Bypass2 set 1 for not set PLL untill all are set
*ptr->RCC  = ( *ptr->RCC & (~(15<<6))) + (21 << 6) ; // XTAL enable for 16MHz  in main oscillator 
*ptr->RCC2 &= ~(1<<13); // PLL enable
*ptr->RCC2 |= (1<<30); // PLL enable for 400MHz
*ptr->RCC2  = (*ptr->RCC2 & ~(127<<22)) + (4<<22); // bit clear & configuration for 80Mz clock
while ( ((*ptr->RIS) & (1<<6)) == 0) {} //wait for PLL RIS bit 
*ptr->RCC2 &= ~(1<<11) ; //Bypass2 set 0 for not set PLL untill all are set	
}