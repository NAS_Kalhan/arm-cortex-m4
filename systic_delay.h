typedef const struct {

	unsigned long *STCTRL;     //SysTick Control and Status
	unsigned long *STRELOAD;   //SysTick Reload Value
	unsigned long *STCURRENT;  //SysTick Current Value

} systick;

systick address = {

	(unsigned long volatile *)(0xE000E010),
	(unsigned long volatile *)(0xE000E014),
	(unsigned long volatile *)(0xE000E018)
		
};

systick *registor = &address;

void systic_init(void){
	
  (*registor->STCTRL)   = 0x00; //Disable systic
  (*registor->STCTRL)   = 0x05; // enable systic with system clock
	
}

void systic_wait(unsigned long delay){
	(*registor->STRELOAD)  = delay -1; 
  (*registor->STCURRENT) = 0x00;
   while(((*registor->STCTRL) & (1<<16))==0)
		                   {
	                             }	

}

void delay_10ms(unsigned long delay){
unsigned long i;
	for (i=0;i<delay;i++){
	    systic_wait(800000);
	}





}