#include "portE_friendly.h"

void portE_int (void){unsigned long delay;
clock_portE 					|= (1<<4); // Register 136: Run Mode Clock Gating Control Register 2 (RCGC2), offset 0x108  set 4th bit
	delay                = clock_portE;
disable_analog_PE      =(disable_analog_PE & ~(1<<0)) & ~(1<<1) & ~(1<<2)  & ~(1<<3); //Register 21: GPIO Analog Mode Select (GPIOAMSEL), offset 0x528
port_control_PE        =  0x00;
port_direction_PE      =(port_direction_PE  | (1<<0)) | (1<<1) | (1<<2)  | (1<<3);    //Register 2: GPIO Direction (GPIODIR), offset 0x400 set 1 to output
port_alternate_PE      =(port_alternate_PE & ~(1<<0)) & ~(1<<1) & ~(1<<2)  & ~(1<<3); //Register 10: GPIO Alternate Function Select (GPIOAFSEL), offset 0x420
port_digital_en_PE     =(port_digital_en_PE  | (1<<0)) | (1<<1) | (1<<2)  | (1<<3);   //Register 18: GPIO Digital Enable (GPIODEN), offset 0x51C
}