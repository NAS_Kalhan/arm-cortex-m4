

/* PORTE definition   */
#define data_PORTE  			 (*((unsigned long volatile *)0x400243FC))
#define clock_portE				 (*((unsigned long volatile *)0x400FE108))
#define disable_analog_PE  (*((unsigned long volatile *)0x40024528))
#define port_control_PE    (*((unsigned long volatile *)0x40024052))
#define port_direction_PE  (*((unsigned long volatile *)0x40024400))
#define port_alternate_PE  (*((unsigned long volatile *)0x40024420))
#define port_digital_en_PE (*((unsigned long volatile *)0x4002451C))

void portE_int (void);


